# treqspreview README

The **treqspreview** extension allows you to preview PlantUML code that contains traceability information to other related artifacts such as test cases.

## Requirements

PlantUML files must be saved with `.puml` file extension

## Features

- View a UML diagram representation of a PlantUML code with trace-links

## Usage

- Install the extension in `vscode` (the extension is available at the vscode marketplace)
- Open a plantuml file in visual studio code
- Right click on the opened plantuml file
- Select "Treqs preview...". Then, you will see the diagram in a new tab.

## Extension Settings

This extension contributes the following settings:

- `treqspreview.enable`: enable/disable this extension

## Release Notes

### Version 1.0.0

- Initial release

### Version 1.1.0

- minor revision
  
### Version 1.2.0

- minor revision
  
### Version 1.3.0

- Minor revision
