# Change Log of treqspreview

## Releases

### Version 1.0.0

- Initial release

### Version 1.1.0

- Small diagram size issue fixed to be scaled with screen size
  
### Version 1.2.0

- Trace-link button disappearing on hiding view issue fixed

### Version 1.3.0

- README file updated
