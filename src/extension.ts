// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import fs = require("fs");
import { basename, dirname } from 'path';
const pako = require('pako');
const utf8bytes = require('utf8-bytes');
let allFiles: any = [];
let currentId = '';

function encode64(data: any) {
    let r = "";
    for (let i = 0; i < data.length; i += 3) {
        if (i + 2 === data.length) {
            r += append3bytes(data.charCodeAt(i), data.charCodeAt(i + 1), 0);
        } else if (i + 1 === data.length) {
            r += append3bytes(data.charCodeAt(i), 0, 0);
        } else {
            r += append3bytes(data.charCodeAt(i), data.charCodeAt(i + 1),
                data.charCodeAt(i + 2));
        }
    }
    return r;
}

function append3bytes(b1: number, b2: number, b3: number) {
    let c1 = b1 >> 2;
    let c2 = ((b1 & 0x3) << 4) | (b2 >> 4);
    let c3 = ((b2 & 0xF) << 2) | (b3 >> 6);
    let c4 = b3 & 0x3F;
    let r = "";
    r += encode6bit(c1 & 0x3F);
    r += encode6bit(c2 & 0x3F);
    r += encode6bit(c3 & 0x3F);
    r += encode6bit(c4 & 0x3F);
    return r;
}

function encode6bit(b: number) {
    if (b < 10) {
        return String.fromCharCode(48 + b);
    }
    b -= 10;
    if (b < 26) {
        return String.fromCharCode(65 + b);
    }
    b -= 26;
    if (b < 26) {
        return String.fromCharCode(97 + b);
    }
    b -= 26;
    if (b === 0) {
        return '-';
    }
    if (b === 1) {
        return '_';
    }
    return '?';
}

function getPlantUMLText() {
    let textContent = '';
    if (vscode.workspace.workspaceFolders) {
        const currentWindow = vscode.window.activeTextEditor;
        if (currentWindow) {
            textContent = currentWindow.document.getText();
        }
    }
    return textContent;
}

function getURL(data: string) {
    let codedText = '';
    let url = '';
    //Encode the text in utf-8
    data = utf8bytes(data);
    let deflated = pako.deflate(data, { level: 9, to: 'string', raw: true });
    codedText = encode64(deflated);
    url = "http://www.plantuml.com/plantuml/img/" + codedText;
    return url;
}
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    let disposable = vscode.commands.registerCommand('extension.treqspreview', () => {
        const panel = vscode.window.createWebviewPanel(
            'treqspreview', // Identifies the type of the webview. Used internally
            'Plantuml preview', // Title of the panel displayed to the user
            vscode.ViewColumn.One, // Editor column to show the new webview panel in.
            {
                enableScripts: true,
                retainContextWhenHidden: true
            } // Webview options. More on these later.
        );
        panel.webview.html = getPreviewData();
        let activeFile = getActiveFile();
        let stories = getTraces(activeFile, "story");
        let tests = getTraces(activeFile, "test");
        let qualities = getTraces(activeFile, "quality");
        const ws = getDir();
        if (ws) {
            const workspacePath = ws.uri.fsPath;
            panel.webview.postMessage({ tracedStories: stories });
            panel.webview.postMessage({ tracedTests: tests });
            panel.webview.postMessage({ tracedQualities: qualities });
            panel.webview.onDidReceiveMessage(message => {
                currentId = message.clickedId;
                scanFiles(currentId, workspacePath);
            });
        }
    });

    context.subscriptions.push(disposable);
}

function getPreviewData() {
    let pumlText = getPlantUMLText();
    let url = getURL(pumlText);
    return `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Plantuml preview</title>
	<style>
		.tracelink{
			margin:10px;
			span:10px;
			font-weight: bold;
		}
		.tracelink a{
			text-decoration: none;
		}
        
        button{
            background-color:#6495ed;
            border:none;
        }
	</style>
</head>
<body>
<div id="tracelinks">

</div>
    <img src=${url} width="100%" />
    <script>
        window.addEventListener('message', event => {
        const message = event.data; 
        let stories = message.tracedStories;
        let tests = message.tracedTests;
        let qualities = message.tracedQualities;
        let divarea = document.getElementById("tracelinks");    
        if(stories){
            let storyParagraph = document.createElement("p");
            for(let key in stories){
                let span = document.createElement("span");
                span.setAttribute("class","tracelink");
                let button = document.createElement("button");
                let storyText = document.createTextNode(stories[key]);
                button.setAttribute("value",stories[key]);
                button.setAttribute("onclick","getClickedVal(value)");
                button.appendChild(storyText);
                span.append(button);
                storyParagraph.appendChild(span);
            }
            divarea.appendChild(storyParagraph);
        }

        if(tests){
            let testParagraph = document.createElement("p");
            for(let key in tests){
                let span = document.createElement("span");
                span.setAttribute("class","tracelink");
                let button = document.createElement("button");
                let testText = document.createTextNode(tests[key]);
                button.setAttribute("value",tests[key]);
                button.setAttribute("onclick","getClickedVal(value)");
                button.appendChild(testText);
                span.append(button);
                testParagraph.appendChild(span);
            }
            divarea.appendChild(testParagraph);
        }

        if(qualities){
            let qualityParagraph = document.createElement("p");
            for(let key in qualities){
                let span = document.createElement("span");
                span.setAttribute("class","tracelink");
                let button = document.createElement("button");
                let qualityText = document.createTextNode(qualities[key]);
                button.setAttribute("value",qualities[key]);
                button.setAttribute("onclick","getClickedVal(value)");
                button.appendChild(qualityText);
                span.append(button);
                qualityParagraph.appendChild(span);
            }
            divarea.appendChild(qualityParagraph);
        }
    });
    
    const vscode = acquireVsCodeApi();
    function getClickedVal(val){
        vscode.postMessage({clickedId:val});
    }
    
    </script>
</body>
</html>`;
}

function getTraces(activeFile: any, tagName: string) {
    let traces: RegExpMatchArray | null = [];
    var lines = fs.readFileSync(activeFile).toString().split("\n");
    for (let i = 0; i < lines.length; i++) {
        let reqTag = lines[i].match(/\[requirement\s+.*?\]/gs);
        if (reqTag) {
            let foundTag = reqTag[0];
            if (tagName === "story") {
                traces = foundTag.match(/story=(.*?)(\s|\])/);
                if (traces) {
                    traces = traces[1].split(",");
                }
            }
            if (tagName === "test") {
                traces = foundTag.match(/test=(.*?)(\s|\])/);
                if (traces) {
                    traces = traces[1].split(",");
                }
            }
            if (tagName === "quality") {
                traces = foundTag.match(/quality=(.*?)(\s|\])/);
                if (traces) {
                    traces = traces[1].split(",");
                }
            }
        }
    }
    return traces;
}
function getDir() {
    if (vscode.workspace.workspaceFolders) {
        const editor = vscode.window.activeTextEditor;
        if (editor) {
            const resource = editor.document.uri;
            if (resource.scheme === 'file') {
                const folder = vscode.workspace.getWorkspaceFolder(resource);
                return folder;
            }
        }
    }
}

function scanFiles(id: string, ws: any) {
    if (ws) {
        getWorkspaceFiles(ws);
        for (let f = 0; f < allFiles.length; f++) {
            if (basename(allFiles[f]).startsWith(id.substring(0, 2))) {
                const lines = fs.readFileSync(allFiles[f]).toString().split("\n");
                for (let l in lines) {
                    const m = lines[l].match(/id\s?=(.*?)(\s|\])/);
                    if (m) {
                        const foundId = m[1];
                        if (id === foundId) {
                            if (basename(dirname(allFiles[f])) !== "logs") {
                                vscode.workspace.openTextDocument(allFiles[f]).then(doc => {
                                    vscode.window.showTextDocument(doc, vscode.ViewColumn.Beside, true);
                                });
                            }
                        }
                    }
                }
            }
        }
    }
}

function getActiveFile() {
    let activeFile = '';
    if (vscode.workspace.workspaceFolders) {
        const currentWindow = vscode.window.activeTextEditor;
        if (currentWindow) {
            activeFile = currentWindow.document.uri.fsPath;
        }
    }
    return activeFile;
}


let getWorkspaceFiles = function (workspace: any) {
    let files = fs.readdirSync(workspace);
    for (let f in files) {
        let file = workspace + '/' + files[f];
        let stats = fs.statSync(file);
        if (stats.isFile()) {
            if (file.endsWith(".py") || file.endsWith(".md")) {
                allFiles.push(file);
            }
        }
        else if (stats.isDirectory) {
            getWorkspaceFiles(file);
        }
    }
};

// this method is called when your extension is deactivated
export function deactivate() { }
